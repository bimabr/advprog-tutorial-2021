package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.List;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    List<Spell> spells;

    public ChainSpell(List<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        spells.forEach(spell -> {spell.cast();});
    }

    @Override
    public void undo() {
        spells.forEach(
                spell -> {
                    spell.undo();
                }
        );
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
