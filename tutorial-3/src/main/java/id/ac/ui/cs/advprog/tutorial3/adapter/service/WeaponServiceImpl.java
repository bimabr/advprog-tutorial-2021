package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    private boolean hasAdapted = false;
    // TODO: implement me
    @Override
    public List<Weapon> findAll() {

        if (!hasAdapted) {
            // Convert Bow to BowAdapter
            for (Bow bow: bowRepository.findAll()
            ) {
                weaponRepository.save(new BowAdapter(bow));
            }


            // Convert Spellbook to SpellbookAdapter
            for (Spellbook sb: spellbookRepository.findAll()
            ) {
                weaponRepository.save(new SpellbookAdapter(sb));
            }
            hasAdapted = true;
        }

        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        findAll();

        Weapon weapon = weaponRepository.findByAlias(weaponName);
        weaponRepository.save(weapon);

        String attack = attackType == 0 ?
                weapon.normalAttack() : weapon.chargedAttack();
        String attType = attackType == 0 ?
                "(normal attack)" : "(charged attack)";
        String combi = weapon.getHolderName() + " attacked with " +
                weapon.getName() + " " + attType + ": " + attack;

        logRepository.addLog(combi);

    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
