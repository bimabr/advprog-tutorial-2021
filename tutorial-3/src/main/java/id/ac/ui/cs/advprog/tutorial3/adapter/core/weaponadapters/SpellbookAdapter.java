package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean hasCharged;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.hasCharged = false;
    }

    @Override
    public String normalAttack() {
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!hasCharged) {
            hasCharged = true;
            return spellbook.largeSpell();
        }
        hasCharged = false;
        return "Mana is not enough to cast large spell";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }
}
