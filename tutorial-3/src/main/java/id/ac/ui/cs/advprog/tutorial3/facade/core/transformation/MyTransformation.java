package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class MyTransformation implements Transformation {
    private int key;

    public MyTransformation(int key) {
        this.key = key;
    }

    public MyTransformation() {
        this(5);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean isEncode) {
        int selector = isEncode ? 1 : -1;
        int newKey = selector * key;

        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        char[] res = new char[n];
        int totalChar = codex.getCharSize();
        for (int i = 0; i < n; i++) {
            int oldIndex = codex.getIndex(text.charAt(i));
            int newIndex = oldIndex + newKey;
            newIndex = newIndex > -1 ? newIndex % totalChar: codex.getCharSize() + newIndex;
            res[i] = codex.getChar(newIndex);
        }
        return new Spell(new String(res), codex);
    }
}
