package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.List;

public class UnifiedTransformation implements Transformation {
    List<Transformation> transformations;

    public UnifiedTransformation() {
        transformations = new ArrayList<>();
        transformations.add(new AbyssalTransformation());
        transformations.add(new CelestialTransformation());
        transformations.add(new MyTransformation());
    }

    @Override
    public Spell encode(Spell spell) {
        for (Transformation t: transformations
             ) {
            spell = t.encode(spell);
        }
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return spell;
    }

    @Override
    public Spell decode(Spell spell) {
        for (int i = transformations.size()-1; i >= 0; i--) {
            spell = transformations.get(i).decode(spell);
        }
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        return spell;
    }
}
