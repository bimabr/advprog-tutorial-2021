package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean isCharged;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.isCharged = false;
    }

    @Override
    public String normalAttack() {
        if (isCharged)
            return bow.shootArrow(true);
        return bow.shootArrow(false);
    }

    @Override
    public String chargedAttack() {
        if (isCharged) {
            isCharged = false;
            return "Leaving aim shot mode";
        }
        isCharged = true;
        return "Entered aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
