package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MyTransformationTest {
    private Class<?> myTransformationClass;

    @BeforeEach
    public void setup() throws Exception {
        myTransformationClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.MyTransformation");
    }

    @Test
    public void testMyTHasEncodeMethod() throws Exception {
        Method translate = myTransformationClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMyTEncodesCorrectly() throws Exception {
        String text = "ABCDEFGH";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "FGHIJKLM";

        Spell result = new MyTransformation().encode(spell);
        System.out.println(result);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "ABCDEFGH";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "GHIJKLMN";

        Spell result = new MyTransformation(6).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTHasDecodeMethod() throws Exception {
        Method translate = myTransformationClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMyTDecodesCorrectly() throws Exception {
        String text = "FGHIJKLM";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "ABCDEFGH";

        Spell result = new MyTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "GHIJKLMN";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "ABCDEFGH";

        Spell result = new MyTransformation(6).decode(spell);
        assertEquals(expected, result.getText());
    }
}
