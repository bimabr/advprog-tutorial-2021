package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UnifiedTransformationTest {
    private Class<?> unifiedTransformationClass;

    @BeforeEach
    public void setup() throws Exception {
        unifiedTransformationClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.UnifiedTransformation");
    }

    @Test
    public void testUnifiedTransformationHasEncodeMethod() throws Exception {
        Method translate = unifiedTransformationClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testUnifiedTransformationEncodesCorrectly() throws Exception {
        String text = "ABCDEFGH";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "q<uxr<,y";

        Spell result = new UnifiedTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testUnifiedTransformationHasDecodeMethod() throws Exception {
        Method translate = unifiedTransformationClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testUnifiedTransformationDecodesCorrectly() throws Exception {
        String text = "q<uxr<,y";
        Codex codex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "ABCDEFGH";

        Spell result = new UnifiedTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

}
