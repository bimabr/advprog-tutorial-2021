package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        //ToDo: Complete Me
    }

    @Override
    public void update() {
        if (!this.guild.getQuestType().equals("E"))
            this.getQuests().add(this.guild.getQuest());
    }

    //ToDo: Complete Me
}
