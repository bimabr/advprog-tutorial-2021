package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "I'm front-liner!!";
    }

    @Override
    public String getType() {
        return "Perisai";
    }
    //ToDo: Complete me
}
