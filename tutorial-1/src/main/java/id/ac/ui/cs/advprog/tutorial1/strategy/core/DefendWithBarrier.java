package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    @Override
    public String defend() {
        return "No one can touch me";
    }

    @Override
    public String getType() {
        return "Barrier";
    }
    //ToDo: Complete me
}
